Some time ago, I wrote a small Python script, which should escape the special-chars (like ',",\n). I wanted to copy my MicroPython sources and paste it into the REPL to save it on my MicroPython-device.


Over time, it got some more features:
 - it tries to recude the length of code (-r)
 - transfer files to device (-d)
 - also it tries to find and copy all imports (-i)
 - copy files from micropython-device (-g)



## Easy install:
It is recommended to use escaper_1FE.tgz.
Download and extract with:
```
wget https://gitlab.com/butzel/escaper.py/raw/master/escaper_0.6_1FE.tgz # download
tar xf escapery * tgz # extract
chmod + x escaper.py # make executable
```
## Usage 


|Option| long                         | Description|
|:------|:----------------------------|--------------------------------------|
|-h      | --help                     |shows this help |
| -l      | --licence            | shows licence (GPLv2)|
| -L      | --lizenz             | shows german translation of GPLv2|
| -v      | --verbose          |   verbose mode|
| -r      | --reduce             | try to reduce code *experimental*|
| -i      | --import             | try to import neccessary files *experimental*|
| -c file | --code=file        |   code file|
| -o file | --out=file          |  write to file (else write to std out)|
| -m file | --mp=file         |    create code directly for (micropython) REPL|
| -d dev  | --dev=dev             |write directly to dev  (e.g. /dev/ttyUSB0)|
| -b baud | --baud=baud           |set the baudrate for communication with --dev|
| -x      | --reset               |reset the device __import__('machine').reset()|
| -g file | --get=file            |read file from micropython-devices|
|         | --read=file           |alias for --get|

``` sh
escaper.py --help
```

### Examples

 - generate MicroPython-Code for pasting in REPL to save "code.py" as "lib.py"
 ```sh
escaper.py -c code.py -m lib.py 
```


 - generate MicroPython-code to save source.py  on your micropython-device
 ```sh
escaper.py -c source.py -m source.py  # or 
escaper.py -mc source.py  # short variant 
```

 - copy code.py to the USB-connected MicroPython-Device 
 ```sh
escaper.py -c code.py -d /dev/ttyUSB0 -b 115200  # or 
escaper.py code.py /dev/ttyUSB0 115200 # short variant of above line
 ```
 - reduce python-code and copy it to the USB-connected MicroPython-Device  
 ``` sh    
escaper.py -r code.py /dev/ttyUSB3  
 ```
 - try to copy full project to the USB connected MicroPython-Device:
 ``` sh    
escaper.py -ir main.py /dev/ttyUSB3  #  very short with code reduction
 ```


## License
GPLv2: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html