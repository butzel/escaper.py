# -*- coding: UTF-8 -*-
__author__ = 'butzel - www.butzel.info'
__license__ = "http://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
__year__ = "2015"

from sys import argv, exit, stdin, stdout, stderr
import license


class MainTools():
    """
    This Class can be used to Parse commandline parameters
    """
    name = ""
    params = []
    values = []

    def __init__(self, sysargs=argv):
        self._error = ""
        self._console = ""
        self.params = sysargs
        self.name = sysargs[0]
        if '/' in self.name:
            self.name = self.name[self.name.rindex('/')+1:]

        for i in range(1, len(self.params)):
            if not self.params[i].startswith("-"):
                self.values.append(self.params[i])

    def hasOption(self, parameter_long, parameter_short):
        """
        :param parameter_long:  e.g. "help"
        :param parameter_short: e.g. 'h'
        :return: returns true if parameter included
        """
        for parameter in self.params:
            if parameter.startswith("--"):
                if parameter.startswith("--"+parameter_long):
                    return True

            elif parameter.startswith("-"):
                if parameter_short in parameter:
                    return True

        return False

    def getOptionValue(self, parameter_long, parameter_short):
        """
        returns the parameter value
        :param parameter_long:
        :param parameter_short:
        :return:
        """
        for i in range(0, len(self.params)):
            parameter = ""
            parameter = self.params[i]
            if parameter.startswith("--"):
                if parameter.startswith("--"+parameter_long):
                    try:
                        return parameter.split("=")[1]
                    except:
                        pass

            elif parameter.startswith("-"):
                if parameter_short in parameter:
                        try:
                            return parameter.split("=")[1]
                        except:
                            try:
                                return self.params[i+1]
                            except:
                                pass
        return ""

    def countArguments(self):
        """
        counts the Parameters of this script
        $0 is not counted
        :return:
        """
        return len(self.params)-1

    def getName(self):
        """
        returns the name of the called script ($0)
        :return: $0
        """
        return self.name

    def getValue(self, pos=None):
        """
        non option values
        :param pos:
        :return:
        """
        try:
            if not pos:
                retval = ""
                for x in self.values:
                    retval += x + " "
            else:
                retval = self.values[pos]
            return retval
        except:
            pass

    def countValues(self):
        return len(self.values)

    @staticmethod
    def readStdIn(readuntil=None, readmax=None):
        retval = ''
        for b in stdin.read():
            retval += b
            if readuntil and retval.endswith(readuntil):
                return retval
            if readmax and readmax >= len(retval):
                return retval
        return retval

    @staticmethod
    def writeStdOut(data):
        stdout.write(data)

    @staticmethod
    def writeStdErr(data):
        stderr.write(data)

    @staticmethod
    def exit(value=0):
        exit(value)

    @staticmethod
    def printLicense(short=True, version=2, german=False):
        """
        returns the selected GPL
        :param short: just the short text (true) or the full license (false)
        :param version: GPL version 2 or 3
        :param german: return the (inofficial) german translation?
        """
        if(version == 3):
            if(short):
                return license.GPLv3short_german() if german else license.GPLv3short()
            else:
                return license.GPLv3_german() if german else license.GPLv3()
        else:  # GPLv2
            if(short):
                return license.GPLv2short_german() if german else license.GPLv2short()
            else:
                return license.GPLv2_german() if german else license.GPLv2()
