#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import MainTools
__author__ = 'butzel'
__license__ = "GPLv2: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html"

VERSION = "0.5"
SERIAL = False


def help(name):
    global SERIAL
    retval = name + " - " + VERSION + """
transfers pyhton-code for interactive saving
===== HELP =====
Option                          Desc
 -h      | --help                shows this help
 -l      | --licence             sho#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import butzelutils.MainTools as MainTools
from os.path import isdir, isfile, getsize
from os import listdir, chdir
__author__ = 'butzel'
__license__ = "GPLv2: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html"

VERSION = "0.6"
SERIAL = False


def help(name):
    global SERIAL
    retval = name + " - " + VERSION + """
transfers pyhton-code for interactive saving
===== HELP =====
Option                          Desc
 -h      | --help                shows this help
 -l      | --licence             shows licence (GPLv2)
 -L      | --lizenz              shows german translation of GPLv2
 -v      | --verbose             verbose mode

 -r      | --reduce              try to reduce code *experimental*
 -i      | --import              try to import neccessary files *experimental*
 -c file | --code=file           code file
 -o file | --out=file            write to file (else write to std out)
 -m file | --mp=file             create code directly for (micropython) REPL
                                 file is the filename on the target-system
                                 like this example:
                                  fhandle = open(<file>,"w")
                                  fhandle.write(<code>)
                                  fhandle.close()
"""
    if SERIAL:
        retval += """
 -d dev  | --dev=dev             write directly to dev  (e.g. /dev/ttyUSB0)
 -b baud | --baud=baud           set the baudrate for communication with --dev
                                  default baudrate for micropython is 115200
 -x      | --reset               reset the device __import__('machine').reset()
 -g file | --get=file            read file from micropython-devices
         | --read=file           alias for --get
 -u      | --upload=file         upload file (or directory) in "binary" mode
"""

    retval += """
==== Examples ====
""" + name + """ -c code.py -m lib.py
""" + name + """ -mc source.py  # short variant of -m source.py -c source.py
"""
    if not SERIAL:
        retval += """
==== pyserial ====
Missing PySerial-Module,
so there are no direct programming to MicroPython available.
 try:
    emerge dev-python/pyserial      # for Gentoo, or
    apt-get install python3-serial  # for Debian, or try:
    pip3 install pyserial           # Pip Installs Python
"""
    else:
        retval += name + """ -c code.py -d /dev/ttyUSB0 -b 115200
""" + name + """ code.py /dev/ttyUSB0 115200 # short variant of above line
""" + name + """ -r code /dev/ttyUSB3  #  very short with code reduction
"""
    print(retval)


def getimports(data):
    retval = ""
    for line in data.split('\n'):
        ifile = ""
        if "import" in line:
            if "from" in line:
                ifile = line[line.index("from") + 5:]
                ifile = ifile[:ifile.index("import")]
            elif "__import__" in line:
                ifile = line[line.index("__import__(") + 12:]
                ifile = ifile[:ifile.index(")")-1]
            else:
                ifile = line[line.index("import") + 7:]
                import re
                ifile = re.sub("#[a-zA-Z 0-9!\-:\.+@§$%&/]*$",
                               '', ifile, flags=re.MULTILINE)
        if ifile:
            if ifile not in retval:
                retval += ifile + ","
    return retval


def getimportablefiles(impcsv):
    """
    filters every importable file
    impcsv = comma sep. value ( mylib, nextlib, time, machine, )
    """
    sep = __import__('os').sep
    from os.path import isfile
    retval = []
    for lib in impcsv.split(','):
        if lib and lib not in retval:
            if isfile(lib.replace('.', sep) + ".py"):
                retval.append(lib.replace('.', sep))
    return retval


def getmpfile(device, filename, verbose=False):
    retval = ""
    blocksize = 512
    serrw(device, "fh = open('" + filename + "', 'r')", True)
    serrw(device, "fh.seek(0)", True)
    readblk = "fh.read(" + str(blocksize) + ")"
    tmp = serrw(device, readblk, True).strip()
    tmp = tmp[len(readblk):-6].strip()
    tmp = tmp[tmp.index("'")+1:]
    retval = tmp
    if verbose:
        print("Download", end=' ', flush=True)
    while tmp:
        tmp = serrw(device, readblk, True)
        tmp = tmp[len(readblk):-6].strip()
        tmp = tmp[tmp.index("'")+1:-1].strip()
        if verbose:
            print('.', end=' ', flush=True)
        wait(.1)
        retval += tmp
    serrw(device, "fh.()")
    if verbose:
        print(": ready", flush=True)
    retval = retval.replace('\\n', '\n').replace('\\r', '\r')
    retval = retval.replace('\\\\', '\\')

    return retval


def writeBinary(device, fname, verbose=False):
    """
    upload a file in Binary-Mode
    slow :-(
    """
    retval = ""
    blocksize = 64
    loop = True
    vcnt = getsize(fname)
    retval = serrw(device, "fh = open('" +
                   fname.split("/")[-1] +
                   "', 'wb')", True)
    if verbose:
        print("upload: '" + fname + "' " + str(vcnt) + "bytes", flush=True)
    with open(fname, "rb") as fh:
        vcnt = 0
        while loop:
            if verbose:
                print(".", flush=True, end="")
            readblk = fh.read(blocksize)
            if readblk:
                serrw(device, "ba = bytearray()", True)
                for i in readblk:
                    serrw(device, "ba.append(" + str(i) + ")", True, 0.01)
                retval += serrw(device, "fh.write(ba)", True)
                serrw(device, "fh.flush()", True)
                if verbose:
                    print(":", flush=True, end="")
                    vcnt += len(readblk)
                loop = len(readblk) == blocksize
            else:
                loop = False
        retval += serrw(device, "fh.close()", True)
        device.flush()
        if verbose:
            print(" " + str(vcnt) + "bytes sent", flush=True)


def uploadDir(device, directory, verbose, parent="."):
    """ recusively upload directory in "binary" mode """
    if verbose:
        print("directory upload:", directory)
    for d in listdir(directory):
        chdir(directory)
        if isfile(d):
            wait(.1)
            writeBinary(device, d, verbose)
        elif isdir(d):
            if verbose:
                print("mkdir", d)
            serrw(device, "__import__('os').mkdir('" + d + "')", True)
            wait(.3)
            serrw(device, "__import__('os').chdir('" + d + "')", True)
            uploadDir(device, d, verbose, d)
            device.flush()
            if verbose:
                print("cd .. ")
            serrw(device, "__import__('os').chdir('..')", True)
        if not directory == '.':
            chdir("..")
        wait(.2)


def wait(sec=.5):
    __import__('time').sleep(sec)


def reduce(data):
    """
    try to recude the code - it's just experimental
     this function
     - remove empty lines
     - remove comment lines
     - change double linebreaks to single linebreaks
     - switches tabs to four spaces (!!)
     - shrink from 4 spaces to one space (!!)
     - remove spaces before and after operators
    """
    import re
    rec = "# -*- coding: UTF-8 -*-" if "# -*- coding:" in data else ""
    data = re.sub("^[ ]*$", '', data, flags=re.MULTILINE)
    data = re.sub("^[ ]*\# .*", '', data, flags=re.MULTILINE)
    data = re.sub("#[a-zA-Z 0-9!\-:\.+@§$%&/]*$", '', data, flags=re.MULTILINE)
    data = re.sub("\n\n", '\n', data, flags=re.MULTILINE)
    # geht besser, aber reichte vorerst:
    data = re.sub("^" + " " * 4, ' ', data.replace('\t', "    "),
                  flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 8, ' ' + ' ' * 4, data, flags=re.MULTILINE)
    data.replace(" == ", "==").replace(" ==", "==").replace("== ", "==")
    for i in "=+-/*<>,())":
        data = data.replace(" " + i + " ", i)
        data = data.replace(" "+i, i).replace(i+" ", i)
    return rec + "\n" + data


def createDirs(filename):
    retval = ""
    sep = __import__('os').sep
    if sep in filename:
        tmp = ""
        crlf = "\r\n"
        for i in filename.split(sep):
            if not filename.endswith(i):
                tmp += i + sep
                retval += '__import__("os").mkdir("' + tmp[:-len(sep)] + '")'
                retval += crlf
    return retval


def write2file(filename, data):
    """ writes data into filename """
    w2file = open(filename, 'w')
    w2file.write(data)
    w2file.close()


def write2console(filename, data, mt):
    """ write the result from escaper to stdout
    filename: if set generate µPy-code "open(filename), write,close"
    data: result of escaper
    mt: instance of MainTools for write to stdout
    """
    if filename:
        prn = createDirs(filename) + "\n"
        prn += "fh = open('" + filename + "', 'w')\nfh.write('" + data + "')"
        prn += "\nfh.flush()\nfh.close()\n"
        data = prn
    mt.writeStdOut("# Start: " + filename + " #\n")
    mt.writeStdOut(data)
    mt.writeStdOut("# End - #\n")


def write2serial(device, filename, data, verbose=False):
    """ writes the result from escaper to serial-device
    device: micropython device (pyserial)
    filename: filename to store result from escaper on µPython
    data: result of escaper's work
    """
    retval = ""
    blocksize = 1024 * 128
    dirs = createDirs(filename)

    retval += serrw(device, "")

    if verbose:
        print("# start transmission: ", end="")

    if dirs:  # try to create directories...
        if verbose:
            print("# create Directories")
        retval += serrw(device, dirs, True)

    retval += serrw(device, "")

    #  open file for write
    msg = "fh = open('" + filename + "', 'w')"
    retval += serrw(device, msg)
    reesc = ''
    # write data in blksize & flush
    for i in range(0, len(data), blocksize):
        msg = reesc + data[i: i + blocksize]
        if msg.strip().endswith('\\'):
            msg = msg[:-1]
            reesc = '\\'
        else:
            reesc = ''
        msg = "fh.write('" + msg + "')"
        retval += serrw(device, msg)
        # wait(.1)
        if verbose:
            print(".", end=" ", flush=True)
        msg = "fh.flush()"
        retval += serrw(device, msg)
        # wait(0.1)

    if verbose:
        print(":")

    #  close file
    msg = "fh.close()"
    retval += serrw(device, msg)

    wait(0.1)
    retval += serrw(device, "# Ready.")
    return retval


def serrw(device, msg, send=True, waits=.001):
    """ serial read & write
    device: pyserial - device
    msg: message/data to write
    send: boolean if true it appends an crlf=\r\n
    """
    crlf = "\r\n"
    if send:
        msg = msg + crlf
    retval = ""
    retval += device.read_all().decode("UTF-8") + "\n"
    wait(waits)
    device.write((msg).encode("UTF-8"))
    wait(waits)
    retval += device.read_all().decode("UTF-8") + "\n"
    return retval


def escaper(data):
    """
    the original idea of escaper.py
    just escape codelines to one string-line
    to copy'n'paste it onto micropython
    """
    data = data.replace('\\', '\\\\').replace('\n', "\\n")
    data = data.replace('\r', "\\r")
    data = data.replace('\'', "\\'")
    return data


def main():
    """ let the fun begin"""
    global SERIAL
    try:
        import serial
        SERIAL = True
    except ImportError:
        SERIAL = False

    mt = MainTools.MainTools()
    if mt.hasOption("help", "h"):
        help(mt.getName())
        mt.exit()

    if mt.hasOption("licence", "l"):
        print(MainTools.MainTools.printLicense(short=False))
        mt.exit()

    if mt.hasOption("lizenz", "L"):
        print(MainTools.MainTools.printLicense(short=False, german=True))
        mt.exit()

    # Settings
    verbose = False
    codefile = ""
    outfile = ""
    outdev = ""
    outbaud = 115200
    mpfile = ""
    mpreset = mt.hasOption("reset", "x")
    getfile = ""
    imports = 0  # 1 = real try, 2 => show only escaper-syntax
    upload = None

    if mt.hasOption("import", 'i'):
        imports = 1
    elif mt.hasOption("IMPORT", "I"):
        imports = 2

    if mt.hasOption("get", "g"):
        getfile = mt.getOptionValue("get", "g")
    elif mt.hasOption("read", "g"):
        getfile = mt.getOptionValue("read", "g")

    if mt.hasOption("verbose", "v"):
        print(mt.getName() + "   Version: " + VERSION)
        print(MainTools.MainTools.printLicense())
        print("\n")
        verbose = True

    if mt.hasOption("code", "c"):
        codefile = mt.getOptionValue("code", "c")

    if mt.hasOption("out", "o"):
        outfile = mt.getOptionValue("out", 'o')

    if mt.hasOption("dev", "d"):
        outdev = mt.getOptionValue("dev", 'd')

    if mt.hasOption("baud", "b"):
        try:
            outbaud = int(mt.getOptionValue("baud", "b"))
        except ValueError:
            print("\nError:\nbaudrate must be an integer!")
            print("typically:\n\t115200\n\t2400\n\t9600")
            print("\t38400\n\t57600\n\t460800\n")
            mt.exit()

    if mt.hasOption("mp", "m"):
        mpfile = mt.getOptionValue("mp", "m")
        if not mpfile:
            if '/' in codefile:
                mpfile = codefile[codefile.rindex("/")+1:]
            else:
                mpfile = codefile

    if mt.hasOption("upload", "u"):
        upload = mt.getOptionValue("upload", "u")

    elif not codefile and not getfile:
        # alternative syntax:
        # escaper src.py /dev/device [baud]
        # escaper src.py target.py
        args = mt.getValue().split(' ')
        if len(args) == 4:
            codefile = args[0]
            outdev = args[1]
            outbaud = args[2]

        elif len(args) == 3:
            codefile = args[0]
            if args[1].startswith("/dev/"):
                outdev = args[1]
            else:
                outfile = args[1]
        if codefile and not mpfile:
            if '/' in codefile:
                mpfile = codefile[codefile.rindex("/")+1:]
            else:
                mpfile = codefile

    if codefile:
        if verbose:
            print("\n")
            print("#")
            print("# Verbose mode        [-v]")
            print("# source code file    [-c]", codefile)
            print("# reduce code         [-r]", mt.hasOption("reduce", "r"))
            if mpfile:
                print("# filename on µPython [-m]", mpfile)
            if outdev:
                print("# target device       [-d]", outdev)
                print("# device baudrate     [-b]", outbaud)
                print("# device reset        [-x]", mpreset)
            if outfile:
                print("# target file         [-o]", outfile)
            print("#\n")

        fhandle = open(codefile, "r")
        data = fhandle.read()
        fhandle.close()

        if mt.hasOption('reduce', 'r'):
            if verbose:
                print("# original size:              ", len(data))
            data = reduce(data)
            if verbose:
                print("# reduced size:               ", len(data))

        if imports:
            impfiles = getimportablefiles(getimports(data))
            for i in impfiles:
                print("# ## import: ", i + ".py ")
                call = mt.getName() + ' -ic ' + i + ".py"
                if verbose:
                    call += " -v"
                if mt.hasOption('reduce', 'r'):
                    call += " -r"
                if outfile:
                    call += " -o " + outfile + "_" + i
                if outdev:
                    call += " -d " + outdev + " -b " + str(outbaud)
                if mpfile or outdev:
                    call += " -m " + i + ".py"
                if imports == 2:  # -I
                    print(" ", call)
                elif imports == 1:  # -i
                    __import__('os').system(call)
                print("# ## end of import", i, "\n\n")

        # original idea:
        data = escaper(data)

        if outdev:
            if verbose:
                print("#start#", outdev, "communication-log")
                print("#sending", len(data), "bytes")
                print("#need approx", 2 + int(len(data)*.0032), "sec")
                print("#")
                wait(.1)

            mpdev = serial.Serial(port=outdev, baudrate=outbaud)
            retval = write2serial(mpdev, mpfile, data, verbose)
            mpdev.flush()
            if mpreset:
                serrw(mpdev, "__import__('machine').reset()")
                mpdev.flush()
            mpdev.close()
            if verbose:
                wait(.1)
                print(retval)
                print("##end##", outdev, "communication-log")

        if outfile:
            write2file(outfile, data)
            if not verbose:
                print(mt.printLicense())
                print("\n")
        elif not outdev:
            if verbose:
                print("# end of verbose data\n#")
            write2console(mpfile, data, mt)

    elif getfile:
        if not mt.hasOption("device", 'd'):
            mt.writeStdOut(MainTools.MainTools.printLicense())
            mt.writeStdErr("\n\nError:")
            mt.writeStdErr("\n missig device parameter: '-d | --dev'")
            mt.writeStdErr("\n  try " + mt.getName() + " --help")
            mt.exit(-4)

        if verbose:
            print("\n")
            print("#")
            print("# Verbose mode        [-v]", verbose)
            if mpfile:
                print("# filename on µPython [-g]", getfile)
            if outdev:
                print("# target device       [-d]", outdev)
                print("# device baudrate     [-b]", outbaud)
                print("# device reset        [-x]", mpreset)
            if outfile:
                print("# target file         [-o]", outfile)
            print("#\n")
            print("#start#", outdev, "communication-log")
            print("#")
            wait(.1)

        mpdev = serial.Serial(port=outdev, baudrate=outbaud)
        data = getmpfile(mpdev, getfile, verbose)
        mpdev.flush()

        if mpreset:
            serrw(mpdev, "__import__('machine').reset()")
            mpdev.flush()

        mpdev.close()
        if verbose:
            wait(.1)
            print(retval)
            print("##end##", outdev, "communication-log")

        if outfile:
            write2file(outfile, data)
            if not verbose:
                print(mt.printLicense())
        else:
            if verbose:
                print("# end of verbose data\n#")
            write2console(mpfile, data, mt)

    elif upload:
        mpdev = serial.Serial(port=outdev, baudrate=outbaud)
        if isfile(upload):
            retval = writeBinary(mpdev, upload,  verbose)
            mpdev.flush()
        elif isdir(upload):
            retval = uploadDir(mpdev, upload, verbose, upload)
        else:
            mt.writeStdErr("...it's a bird... it's plane....\n")
            mt.writeStdErr("neither file nor directory:  " + upload + "\n")

        mpdev.close()

    else:
        mt.writeStdOut(MainTools.MainTools.printLicense())
        mt.writeStdErr("\n\n\nError:")
        mt.writeStdErr("\n missing neccessary parameter: '-c | --code'")
        mt.writeStdErr("\n  try " + mt.getName() + " --help")
    mt.writeStdOut("\n")


if __name__ == "__main__":
    main()
ws licence (GPLv2)
 -L      | --lizenz              shows german translation of GPLv2
 -v      | --verbose             verbose mode

 -r      | --reduce              try to reduce code *experimental*
 -i      | --import              try to import neccessary files *experimental*
 -c file | --code=file           code file
 -o file | --out=file            write to file (else write to std out)
 -m file | --mp=file             create code directly for (micropython) REPL
                                 file is the filename on the target-system
                                 like this example:
                                  fhandle = open(<file>,"w")
                                  fhandle.write(<code>)
                                  fhandle.close()
"""
    if SERIAL:
        retval += """
 -d dev  | --dev=dev             write directly to dev  (e.g. /dev/ttyUSB0)
 -b baud | --baud=baud           set the baudrate for communication with --dev
                                  default baudrate for micropython is 115200
 -x      | --reset               reset the device __import__('machine').reset()
 -g file | --get=file            read file from micropython-devices
         | --read=file           alias for --get
"""

    retval += """
==== Examples ====
""" + name + """ -c code.py -m lib.py
""" + name + """ -mc source.py  # short variant of -m source.py -c source.py
"""
    if not SERIAL:
        retval += """
==== pyserial ====
Missing PySerial-Module,
so there are no direct programming to MicroPython available.
 try:
    emerge dev-python/pyserial      # for Gentoo, or
    apt-get install python3-serial  # for Debian, or try:
    pip3 install pyserial           # Pip Installs Python
"""
    else:
        retval += name + """ -c code.py -d /dev/ttyUSB0 -b 115200
""" + name + """ code.py /dev/ttyUSB0 115200 # short variant of above line
""" + name + """ -r code /dev/ttyUSB3  #  very short with code reduction
"""
    print(retval)


def getimports(data):
    retval = ""
    for line in data.split('\n'):
        ifile = ""
        if "import" in line:
            if "from" in line:
                ifile = line[line.index("from") + 5:]
                ifile = ifile[:ifile.index("import")]
            elif "__import__" in line:
                ifile = line[line.index("__import__(") + 12:]
                ifile = ifile[:ifile.index(")")-1]
            else:
                ifile = line[line.index("import") + 7:]
                import re
                ifile = re.sub("#[a-zA-Z 0-9!\-:\.+@§$%&/]*$",
                               '', ifile, flags=re.MULTILINE)
        if ifile:
            if ifile not in retval:
                retval += ifile + ","
    return retval


def getimportablefiles(impcsv):
    """
    filters every importable file
    impcsv = comma sep. value ( mylib, nextlib, time, machine, )
    """
    sep = __import__('os').sep
    from os.path import isfile
    retval = []
    for lib in impcsv.split(','):
        if lib and lib not in retval:
            if isfile(lib.replace('.', sep) + ".py"):
                retval.append(lib.replace('.', sep))
    return retval


def getmpfile(device, filename, verbose=False):
    retval = ""
    blocksize = 512
    serrw(device, "fh = open('" + filename + "', 'r')", True)
    serrw(device, "fh.seek(0)", True)
    readblk = "fh.read(" + str(blocksize) + ")"
    tmp = serrw(device, readblk, True).strip()
    tmp = tmp[len(readblk):-6].strip()
    tmp = tmp[tmp.index("'")+1:]
    retval = tmp
    if verbose:
        print("Download", end=' ', flush=True)
    while tmp:
        tmp = serrw(device, readblk, True)
        tmp = tmp[len(readblk):-6].strip()
        tmp = tmp[tmp.index("'")+1:-1].strip()
        if verbose:
            print('.', end=' ', flush=True)
        wait(.1)
        retval += tmp
    serrw(device, "fh.()")
    if verbose:
        print(": ready", flush=True)
    retval = retval.replace('\\n', '\n').replace('\\r', '\r')
    retval = retval.replace('\\\\', '\\')

    return retval


def wait(sec=.5):
    __import__('time').sleep(sec)


def reduce(data):
    """
    try to recude the code - it's just experimental
     this function
     - remove empty lines
     - remove comment lines
     - change double linebreaks to single linebreaks
     - switches tabs to four spaces (!!)
     - shrink from 4 spaces to one space (!!)
     - remove spaces before and after operators
    """
    import re
    rec = "# -*- coding: UTF-8 -*-" if "# -*- coding:" in data else ""
    data = re.sub("^[ ]*$", '', data, flags=re.MULTILINE)
    data = re.sub("^[ ]*\# .*", '', data, flags=re.MULTILINE)
    data = re.sub("#[a-zA-Z 0-9!\-:\.+@§$%&/]*$", '', data, flags=re.MULTILINE)
    data = re.sub("\n\n", '\n', data, flags=re.MULTILINE)
    # geht besser, aber reichte vorerst:
    data = re.sub("^" + " " * 4, ' ', data.replace('\t', "    "),
                  flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 4, ' ', data, flags=re.MULTILINE)
    data = re.sub("^" + " " * 8, ' ' + ' ' * 4, data, flags=re.MULTILINE)
    data.replace(" == ", "==").replace(" ==", "==").replace("== ", "==")
    for i in "=+-/*<>,())":
        data = data.replace(" " + i + " ", i)
        data = data.replace(" "+i, i).replace(i+" ", i)
    return rec + "\n" + data


def createDirs(filename):
    retval = ""
    sep = __import__('os').sep
    if sep in filename:
        tmp = ""
        crlf = "\r\n"
        for i in filename.split(sep):
            if not filename.endswith(i):
                tmp += i + sep
                retval += '__import__("os").mkdir("' + tmp[:-len(sep)] + '")'
                retval += crlf
    return retval


def write2file(filename, data):
    """ writes data into filename """
    w2file = open(filename, 'w')
    w2file.write(data)
    w2file.close()


def write2console(filename, data, mt):
    """ write the result from escaper to stdout
    filename: if set generate µPy-code "open(filename), write,close"
    data: result of escaper
    mt: instance of MainTools for write to stdout
    """
    if filename:
        prn = createDirs(filename) + "\n"
        prn += "fh = open('" + filename + "', 'w')\nfh.write('" + data + "')"
        prn += "\nfh.flush()\nfh.close()\n"
        data = prn
    mt.writeStdOut("# Start: " + filename + " #\n")
    mt.writeStdOut(data)
    mt.writeStdOut("# End - #\n")


def write2serial(device, filename, data, verbose=False):
    """ writes the result from escaper to serial-device
    device: micropython device (pyserial)
    filename: filename to store result from escaper on µPython
    data: result of escaper's work
    """
    retval = ""
    blocksize = 512 
    dirs = createDirs(filename)

    retval += serrw(device, "")

    if verbose:
        print("# start transmission: ", end="")

    if dirs:  # try to create directories...
        if verbose:
            print("# create Directories")
        retval += serrw(device, dirs, True)

    retval += serrw(device, "")

    #  open file for write
    msg = "fh = open('" + filename + "', 'w')"
    retval += serrw(device, msg)
    reesc = ''
    # write data in blksize & flush
    for i in range(0, len(data), blocksize):
        msg = reesc + data[i: i + blocksize]
        if msg.strip().endswith('\\'):
            msg = msg[:-1]
            reesc = '\\'
        else:
            reesc = ''
        msg = "fh.write('" + msg + "')"
        retval += serrw(device, msg)
        wait(.1)
        if verbose:
            print(".", end=" ", flush=True)
        msg = "fh.flush()"
        retval += serrw(device, msg)
        wait(0.1)

    if verbose:
        print(":")

    #  close file
    msg = "fh.close()"
    retval += serrw(device, msg)

    wait(0.5)
    retval += serrw(device, "# Ready.")
    return retval


def serrw(device, msg, send=True):
    """ serial read & write
    device: pyserial - device
    msg: message/data to write
    send: boolean if true it appends an crlf=\r\n
    """
    crlf = "\r\n"
    if send:
        msg = msg + crlf
    retval = ""
    retval += device.read_all().decode("UTF-8") + "\n"
    wait(0.1)
    device.write((msg).encode("UTF-8"))
    wait(0.1)
    retval += device.read_all().decode("UTF-8") + "\n"
    return retval


def escaper(data):
    """
    the original idea of escaper.py
    just escape codelines to one string-line
    to copy'n'paste it onto micropython
    """
    data = data.replace('\\', '\\\\').replace('\n', "\\n")
    data = data.replace('\r', "\\r")
    data = data.replace('\'', "\\'")
    return data


def main():
    """ let the fun begin"""
    global SERIAL
    try:
        import serial
        SERIAL = True
    except ImportError:
        SERIAL = False

    mt = MainTools.MainTools()
    if mt.hasOption("help", "h"):
        help(mt.getName())
        mt.exit()

    if mt.hasOption("licence", "l"):
        print(MainTools.MainTools.printLicense(short=False))
        mt.exit()

    if mt.hasOption("lizenz", "L"):
        print(MainTools.MainTools.printLicense(short=False, german=True))
        mt.exit()

    # Settings
    verbose = False
    codefile = ""
    outfile = ""
    outdev = ""
    outbaud = 115200
    mpfile = ""
    mpreset = mt.hasOption("reset", "x")
    getfile = ""
    imports = 0  # 1 = real try, 2 => show only escaper-syntax

    if mt.hasOption("import", 'i'):
        imports = 1
    elif mt.hasOption("IMPORT", "I"):
        imports = 2

    if mt.hasOption("get", "g"):
        getfile = mt.getOptionValue("get", "g")
    elif mt.hasOption("read", "g"):
        getfile = mt.getOptionValue("read", "g")

    if mt.hasOption("verbose", "v"):
        print(mt.getName() + "   Version:" + VERSION)
        print(MainTools.MainTools.printLicense())
        verbose = True

    if mt.hasOption("code", "c"):
        codefile = mt.getOptionValue("code", "c")

    if mt.hasOption("out", "o"):
        outfile = mt.getOptionValue("out", 'o')

    if mt.hasOption("dev", "d"):
        outdev = mt.getOptionValue("dev", 'd')

    if mt.hasOption("baud", "b"):
        try:
            outbaud = int(mt.getOptionValue("baud", "b"))
        except:
            print("\nError:\nbaudrate must be an integer!")
            print("typically:\n\t115200\n\t2400\n\t9600")
            print("\t38400\n\t57600\n\t460800\n")
            mt.exit()

    if mt.hasOption("mp", "m"):
        mpfile = mt.getOptionValue("mp", "m")
        if not mpfile:
            if '/' in codefile:
                mpfile = codefile[codefile.rindex("/")+1:]
            else:
                mpfile = codefile

    if not codefile and not getfile:
        # alternative syntax:
        # escaper src.py /dev/device [baud]
        # escaper src.py target.py
        args = mt.getValue().split(' ')
        if len(args) == 4:
            codefile = args[0]
            outdev = args[1]
            outbaud = args[2]

        elif len(args) == 3:
            codefile = args[0]
            if args[1].startswith("/dev/"):
                outdev = args[1]
            else:
                outfile = args[1]
        if codefile and not mpfile:
            if '/' in codefile:
                mpfile = codefile[codefile.rindex("/")+1:]
            else:
                mpfile = codefile

    if codefile:
        if verbose:
            print("\n")
            print("#")
            print("# Verbose mode        [-v]")
            print("# source code file    [-c]", codefile)
            print("# reduce code         [-r]", mt.hasOption("reduce", "r"))
            if mpfile:
                print("# filename on µPython [-m]", mpfile)
            if outdev:
                print("# target device       [-d]", outdev)
                print("# device baudrate     [-b]", outbaud)
                print("# device reset        [-x]", mpreset)
            if outfile:
                print("# target file         [-o]", outfile)
            print("#\n")

        fhandle = open(codefile, "r")
        data = fhandle.read()
        fhandle.close()

        if mt.hasOption('reduce', 'r'):
            if verbose:
                print("# original size:              ", len(data))
            data = reduce(data)
            if verbose:
                print("# reduced size:               ", len(data))

        if imports:
            impfiles = getimportablefiles(getimports(data))
            for i in impfiles:
                print("# ## import: ", i + ".py ")
                call = mt.getName() + ' -ic ' + i + ".py"
                if verbose:
                    call += " -v"
                if mt.hasOption('reduce', 'r'):
                    call += " -r"
                if outfile:
                    call += " -o " + outfile + "_" + i
                if outdev:
                    call += " -d " + outdev + " -b " + str(outbaud)
                if mpfile or outdev:
                    call += " -m " + i + ".py"
                if imports == 2:  # -I
                    print(" ", call)
                elif imports == 1:  # -i
                    __import__('os').system(call)
                print("# ## end of import", i, "\n\n")

        # original idea:
        data = escaper(data)

        if outdev:
            if verbose:
                print("#start#", outdev, "communication-log")
                print("#sending", len(data), "bytes")
                print("#need approx", 2 + int(len(data)*.0032), "sec")
                print("#")
                wait(.1)

            mpdev = serial.Serial(port=outdev, baudrate=outbaud)
            retval = write2serial(mpdev, mpfile, data, verbose)
            mpdev.flush()
            if mpreset:
                serrw(mpdev, "__import__('machine').reset()")
                mpdev.flush()
            mpdev.close()
            if verbose:
                wait(.1)
                print(retval)
                print("##end##", outdev, "communication-log")

        if outfile:
            write2file(outfile, data)
            if not verbose:
                print(mt.printLicense())
        elif not outdev:
            if verbose:
                print("# end of verbose data\n#")
            write2console(mpfile, data, mt)

    elif getfile:
        if not mt.hasOption("device", 'd'):
            mt.writeStdOut(MainTools.MainTools.printLicense())
            mt.writeStdErr("\n\nError:")
            mt.writeStdErr("\n missig device parameter: '-d | --dev'")
            mt.writeStdErr("\n  try " + mt.getName() + " --help")
            mt.exit(-4)

        if verbose:
            print("\n")
            print("#")
            print("# Verbose mode        [-v]", verbose)
            if mpfile:
                print("# filename on µPython [-g]", getfile)
            if outdev:
                print("# target device       [-d]", outdev)
                print("# device baudrate     [-b]", outbaud)
                print("# device reset        [-x]", mpreset)
            if outfile:
                print("# target file         [-o]", outfile)
            print("#\n")
            print("#start#", outdev, "communication-log")
            print("#")
            wait(.1)

        mpdev = serial.Serial(port=outdev, baudrate=outbaud)
        data = getmpfile(mpdev, getfile, verbose)
        mpdev.flush()

        if mpreset:
            serrw(mpdev, "__import__('machine').reset()")
            mpdev.flush()

        mpdev.close()
        if verbose:
            wait(.1)
            print(retval)
            print("##end##", outdev, "communication-log")

        if outfile:
            write2file(outfile, data)
            if not verbose:
                print(mt.printLicense())
        else:
            if verbose:
                print("# end of verbose data\n#")
            write2console(mpfile, data, mt)

    else:
        mt.writeStdOut(MainTools.MainTools.printLicense())
        mt.writeStdErr("\n\n\nError:")
        mt.writeStdErr("\n missing neccessary parameter: '-c | --code'")
        mt.writeStdErr("\n  try " + mt.getName() + " --help")
    mt.writeStdOut("\n")


if __name__ == "__main__":
    main()
